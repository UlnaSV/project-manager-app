<?php

namespace App\Services;

use \DirectoryIterator;
use App\Services\FileInterface;
use App\Models\Task;
use App\Models\Project;
use File;
use App\Models\FileModel;

class ServerFileService implements FileInterface 
{
    public function setFile(object $data)
    {
      $userId = $data->user()->id;
      $project = Project::getProjectByName($data->project, $userId);
      $task = Task::getTaskByProjectId($project->id, $data->task);
      
      $fileName = $data->file('file')->getClientOriginalName();
      $data->file('file')->move(public_path('files'), $fileName);

    FileModel::create([
        'name' => $fileName,
        'task_id' => $task->id
    ]);

    }

    public function getFile($taskId)
    {
        $file = FileModel::getByTaskId($taskId);
        if(!$file) {
            return 'no file';
        }
        $fileName = $file->name;
        
        return asset('files/'.$fileName);
    }

    public function deleteFile($userId, $project, $task)
    {
        $userId = $userId;
        $project = Project::getProjectByName($project, $userId);
        $task = Task::getTaskByProjectId($project->id, $task);

        $file = FileModel::getByTaskId($task->id);
        $fileName = $file->name;
        $file->delete();
        $this->deleteFileFromServer($fileName);
        
    }

    public function deleteFileFromServer($fileName)
    {
        File::delete(public_path('files/'.$fileName));
    }

    public function checkInDB()
    {
        $results = [];

        $dirs = File::directories(public_path());
    
        foreach($dirs as $dir){
          $files = File::files($dir);
          foreach($files as $f){
              $results[] = $f->getRelativePathname(); 
          }
        }
        foreach($results as $result){
            if(!FileModel::where('name', $result)->first()) {
                File::delete(public_path('files/'.$result));
            }
        }
    }
}