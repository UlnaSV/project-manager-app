<?php

namespace App\Services;

interface FileInterface 
{
    public function setFile(object $data);

    public function getFile(int $taskId);

    public function deleteFile(int $userId, string $project, string $task);

}