<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

Class UserService {

    public function register(string $name, string $email, string $password)
    {

        User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password)
        ]);
    }
}