<?php

namespace App\Services;

use App\Models\Task;
use App\Models\Project;
use App\Models\User;
use App\Services\ServerFileService;
use App\Models\FileModel;
use App\Notifications\NotifyUser;


class TaskService 
{
    public function addTask(int $userId, string $project_name, string $name, $description)
    {
        
        $projectId = Project::getProjectByName($project_name, $userId)->id;

        Task::create([
            'name' => $name,
            'status' => 'new',
            'description'=> $description,
            'project_id' => $projectId,
        ]);
    }

    public function getTasks(string $project, int $userId)
    {
        $projectId = Project::getProjectByName($project, $userId)->id;
        
        return Task::getTasksByProjectId($projectId);
    }

    public function getTask(int $userId, string $project, string $task)
    {
        $projectId = Project::getProjectByName($project, $userId)->id;
        $task = Task::getTaskByProjectId($projectId, $task);

        $fileService = new ServerFileService();

        $file = $fileService->getFile($task->id);

        return [$task, $file];
    }

    public function editTask($name, $status, $description, $project, $task, $userId)
    {
        $projectId = Project::getProjectByName($project, $userId)->id;
        $task = Task::getTaskByProjectId($projectId, $task);

        $oldStatus = $task->status;

        if(strcmp($oldStatus, $status) != 0) {
            
            $user = User::where('id', $userId)->first();
            $greeting = 'Hi, '.$user->name;
            $message = 'Status of the '.$task->name.' task in '.$project.' project has been changed from '.$oldStatus.' to '.$status;

            $user->notify(new NotifyUser($greeting, $message));
        }

        $task->name = $name;
        $task->status = $status;
        $task->description = $description;

        $task->save();
    }

    public function deleteTask($userId, $project, $task)
    {
        $projectId = Project::getProjectByName($project, $userId)->id;
        $task = Task::getTaskByProjectId($projectId, $task);

        $file = FileModel::getByTaskId($task->id);
        if($file) {
            $fileName = $file->name;
            $fileService = new ServerFileService();
            $fileService->deleteFileFromServer($fileName);
        }
        $task->delete();
    }
}