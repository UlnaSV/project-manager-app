<?php

namespace App\Services;

use App\Models\Project;

class ProjectService 
{

    public function addProject(object $data)
    {
        $userId = $data->user()->id;

        Project::create([
            'name' => $data->projectName,
            'user_id' => $userId,
        ]);
    }

    public function getProjects(object $data)
    {
        $userId = $data->user()->id;

        return Project::getProjectsByUserId($userId);
    }

    public function editProject($projectName, $oldProjectName, $userId)
    {
        $project = Project::getProjectByName($oldProjectName, $userId);
        $project->name = $projectName;
        $project->save();
    }

    public function deleteProject($project, $userId)
    {
        $project = Project::getProjectByName($project, $userId);

        $project->delete();
    }
}