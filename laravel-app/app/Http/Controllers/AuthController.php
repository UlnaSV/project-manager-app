<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    protected $user;

    public function __construct(UserService $user)
    {
        $this->user = $user;
    }
    public function register(RegisterRequest $request) 
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        $this->user->register($name, $email, $password);
        
        return response('OK',200);
    }

    public function login(LoginRequest $request)
    {
        if(Auth::attempt($request->only('email', 'password'))) {
            return response(Auth::user(), 200);
        }
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are1 incorrect.']
        ]);
    }


    public function logout()
    {
        Auth::logout();
    }
}
