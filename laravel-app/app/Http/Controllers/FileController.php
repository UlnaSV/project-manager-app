<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FileInterface;

class FileController extends Controller
{
    public function __construct(FileInterface $file)
    {
        $this->file = $file;
    }
    public function setFile(Request $request)
    {
        $file = $this->file;
        return $file->setFile($request);
    }

    public function deleteFile(Request $request)
    {
        $userId = $request->user()->id;
        $project = $request->project;
        $task = $request->task;

        $this->file->deleteFile($userId, $project, $task);
    }

    public function checkInDB(Request $request)
    {
       return $this->file->checkInDB();
    }
}
