<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\TaskService;

class TaskController extends Controller
{
    protected $task;

    public function __construct(TaskService $task)
    {
        $this->task = $task;
    }

    public function addTask(Request $request)
    {
        $userId = $request->user()->id;
        $project_name = $request->project_name;
        $name = $request->name;
        $description = $request->description;

        return $this->task->addTask($userId, $project_name, $name, $description);
    }

    public function getTasks(Request $request)
    {
        $userId = $request->user()->id;
        $project = $request->project;

        return $this->task->getTasks($project, $userId);
    }

    public function getTask(Request $request)
    {
        $userId = $request->user()->id;
        $project = $request->project;
        $task = $request->task;

        return $this->task->getTask($userId, $project, $task);
    }

    public function editTask(Request $request)
    {
        $userId = $request->user()->id;
        $project = $request->project;
        $task = $request->task;
        $name = $request->name;
        $status = $request->status;
        $description = $request->description;

        return $this->task->editTask($name, $status, $description, $project, $task, $userId);
    }

    public function deleteTask(Request $request)
    {
        $userId = $request->user()->id;
        $project = $request->project;
        $task = $request->task;
        
        return $this->task->deleteTask($userId, $project, $task);
    }
}
