<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProjectService;

class ProjectController extends Controller
{
    protected $project;

    public function __construct(ProjectService $project)
    {
        $this->project = $project;
    }

    public function addProject(Request $request)
    {
        return $this->project->addProject($request);
    }

    public function getProjects(Request $request)
    {
        return $this->project->getProjects($request);
    }

    public function editProject(Request $request)
    {
        $projectName = $request->project;
        $oldProjectName = $request->oldProject;
        $userId = $request->user()->id;

        return $this->project->editProject($projectName, $oldProjectName, $userId);
    }

    public function deleteProject(Request $request)
    {
        $userId = $request->user()->id;
        $projectName = $request->project;

        return $this->project->deleteProject($projectName, $userId);
    }
}

