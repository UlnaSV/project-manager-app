<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Project;

class Task extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'status',
        'description',
        'project_id'
    ];

    public static function getTasksByProjectId($id)
    {
        return self::where('project_id', $id)->get();
    }

    public static function getTaskByProjectId($id, $name)
    {
        return self::where('project_id', $id)->where('name', $name)->first();
    }

    // public function project()
    // {
    //     return $this->belongsTo(Project::class);
    // }
}
