<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileModel extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'task_id'
    ];

    public static function getByTaskId($id)
    {
        return self::where('task_id', $id)->first();
    }

}
