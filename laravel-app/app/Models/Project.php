<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'user_id'
    ];

    public static function getProjectsByUserId($id)
    {
        return self::where('user_id', $id)->get();
    }

    public static function getProjectByName($name, $userId)
    {
        return self::where('name', $name)->where('user_id', $userId)->first();
    }

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }
}
