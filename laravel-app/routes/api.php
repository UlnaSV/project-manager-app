<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\FileController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('logout', [AuthController::class, 'logout']);

Route::post('/addProject', [ProjectController::class, 'addProject']);
Route::get('/getProjects', [ProjectController::class, 'getProjects']);
Route::put('/editProject', [ProjectController::class, 'editProject']);
Route::delete('/deleteProject', [ProjectController::class, 'deleteProject']);


Route::post('/addTask', [TaskController::class, 'addTask']);
Route::post('/editTask', [TaskController::class, 'editTask']);
Route::delete('/deleteTask', [TaskController::class, 'deleteTask']);
Route::get('/getTasks', [TaskController::class, 'getTasks']);
Route::get('/getTask', [TaskController::class, 'getTask']);

Route::post('/setFile', [FileController::class, 'setFile']);
Route::delete('/deleteFile', [FileController::class, 'deleteFile']);
Route::delete('/checkInDB', [FileController::class, 'checkInDB']);
