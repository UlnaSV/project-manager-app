

Firstly clone the repository.

git clone https://gitlab.com/UlnaSV/project-manager-app.git

The project manager app are two vue SPA and laravel REST API projects.

Laravel-app:

cd laravel-app/ cp env.example .env php artisan key:generate

composer install

php artisan migrate php artisan migrate:refresh

in your gmail account settings->security allow other applications to access your email account.

php artisan serve - to run the app.

Vue-app:

cd vue-app/ npm install in src/api/api.js change baseURL for Laravel-app API.

npm run serve

