import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Project from '../views/Project.vue'
import CreateTask from '../views/CreateTask.vue'
import Task from '../views/Task.vue'


Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            authOnly: true
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            guestOnly: true
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            guestOnly: true
        }
    },
    {
        path: '/:project_name',
        name: 'Project',
        component: Project,
        meta: {
            authOnly: true
        }
    },
    {
        path: '/create-task-in-:project_name',
        name: 'CreateTask',
        component: CreateTask,
        meta: {
            authOnly: true
        }
    },
    {
        path: '/:project_name/:task_name',
        name: 'Task',
        component: Task,
        meta: {
            authOnly: true
        }
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

function isLoggedIn() {
    return localStorage.getItem("auth");
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.authOnly)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!isLoggedIn()) {
            next({
                path: "/login",
                query: {
                    redirect: to.fullPath
                }
            });
        } else {
            next();
        }
    } else if (to.matched.some(record => record.meta.guestOnly)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (isLoggedIn()) {
            next({
                path: "/",
                query: {
                    redirect: to.fullPath
                }
            });
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});

export default router