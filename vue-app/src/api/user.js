import api from "./api"
import Csrf from './csrf';

export default {
    async register(form) {
        await Csrf.getCookie();

        return api.post("/register", form);
    },
    async login(form) {
        await Csrf.getCookie();

        return api.post("/login", form);
    },
    async logout() {
        await Csrf.getCookie();

        return api.post("/logout");
    },

    auth() {
        return api.get("/user");
    },

    async addProject(form) {
        await Csrf.getCookie();

        return api.post('/addProject', form);
    },

    editProject(data) {
        return api.put('/editProject', data);
    },

    deleteProject(project) {
        api.delete('deleteProject', {
            params: {
                project: project
            }
        })
    },

    async getProjects() {
        await Csrf.getCookie();

        return api.get('/getProjects');
    },

    async getTasks(data) {

        return api.get('/getTasks', {
            params: {
                project: data
            }
        });
    },

    getTask(task, project) {
        return api.get('/getTask', {
            params: {
                task: task,
                project: project
            }
        })
    },

    async addTask(form) {
        await Csrf.getCookie();

        return api.post('/addTask', form);
    },

    setFile(data) {
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }

        return api.post('/setFile', data, config)
    },

    deleteFile(task, project) {
        return api.delete('/deleteFile', {
            params: {
                task: task,
                project: project
            }
        });
    },

    editTask(data) {
        return api.post('/editTask', data);
    },

    deleteTask(project, task) {
        return api.delete('/deleteTask', {
            params: {
                project: project,
                task: task
            }
        })
    },

    checkFiles() {
        return api.delete('/checkInDB');
    }

}